/**
 * 
 *
 *
 *
 *
 */

package cn.eggsl.modules.oss.service;

import cn.eggsl.common.page.PageData;
import cn.eggsl.common.service.BaseService;
import cn.eggsl.modules.oss.entity.SysOssEntity;

import java.util.Map;

/**
 * 文件上传
 * 
 * @author Mark sunlightcs@gmail.com
 */
public interface SysOssService extends BaseService<SysOssEntity> {

	PageData<SysOssEntity> page(Map<String, Object> params);
}
