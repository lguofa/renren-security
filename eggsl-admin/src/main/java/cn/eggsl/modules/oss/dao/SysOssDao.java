/**
 *
 *
 *
 *
 *
 */

package cn.eggsl.modules.oss.dao;

import cn.eggsl.common.dao.BaseDao;
import cn.eggsl.modules.oss.entity.SysOssEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传
 * 
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SysOssDao extends BaseDao<SysOssEntity> {
	
}
