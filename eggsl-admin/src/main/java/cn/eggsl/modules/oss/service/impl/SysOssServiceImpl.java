/**
 *
 *
 *
 *
 *
 */

package cn.eggsl.modules.oss.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import cn.eggsl.common.constant.Constant;
import cn.eggsl.common.page.PageData;
import cn.eggsl.common.service.impl.BaseServiceImpl;
import cn.eggsl.modules.oss.dao.SysOssDao;
import cn.eggsl.modules.oss.entity.SysOssEntity;
import cn.eggsl.modules.oss.service.SysOssService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class SysOssServiceImpl extends BaseServiceImpl<SysOssDao, SysOssEntity> implements SysOssService {

	@Override
	public PageData<SysOssEntity> page(Map<String, Object> params) {
		IPage<SysOssEntity> page = baseDao.selectPage(
			getPage(params, Constant.CREATE_DATE, false),
			new QueryWrapper<>()
		);
		return getPageData(page, SysOssEntity.class);
	}
}
