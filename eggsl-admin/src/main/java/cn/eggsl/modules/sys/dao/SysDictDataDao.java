/**
 *
 *
 *
 *
 *
 */

package cn.eggsl.modules.sys.dao;

import cn.eggsl.common.dao.BaseDao;
import cn.eggsl.modules.sys.entity.DictData;
import cn.eggsl.modules.sys.entity.SysDictDataEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 字典数据
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SysDictDataDao extends BaseDao<SysDictDataEntity> {

    /**
     * 字典数据列表
     */
    List<DictData> getDictDataList();
}
