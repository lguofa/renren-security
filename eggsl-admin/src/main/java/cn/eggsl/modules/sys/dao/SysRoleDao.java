/**
 * 
 *
 *
 *
 *
 */

package cn.eggsl.modules.sys.dao;

import cn.eggsl.common.dao.BaseDao;
import cn.eggsl.modules.sys.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色管理
 * 
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SysRoleDao extends BaseDao<SysRoleEntity> {
	

}
