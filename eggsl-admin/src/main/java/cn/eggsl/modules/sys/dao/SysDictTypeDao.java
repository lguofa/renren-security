/**
 *
 *
 *
 *
 *
 */

package cn.eggsl.modules.sys.dao;

import cn.eggsl.common.dao.BaseDao;
import cn.eggsl.modules.sys.entity.DictType;
import cn.eggsl.modules.sys.entity.SysDictTypeEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 字典类型
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SysDictTypeDao extends BaseDao<SysDictTypeEntity> {

    /**
     * 字典类型列表
     */
    List<DictType> getDictTypeList();

}
