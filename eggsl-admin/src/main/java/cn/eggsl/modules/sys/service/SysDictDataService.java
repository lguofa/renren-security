/**
 * 
 *
 *
 *
 *
 */

package cn.eggsl.modules.sys.service;

import cn.eggsl.common.page.PageData;
import cn.eggsl.common.service.BaseService;
import cn.eggsl.modules.sys.dto.SysDictDataDTO;
import cn.eggsl.modules.sys.entity.SysDictDataEntity;

import java.util.Map;

/**
 * 数据字典
 *
 * @author Mark sunlightcs@gmail.com
 */
public interface SysDictDataService extends BaseService<SysDictDataEntity> {

    PageData<SysDictDataDTO> page(Map<String, Object> params);

    SysDictDataDTO get(Long id);

    void save(SysDictDataDTO dto);

    void update(SysDictDataDTO dto);

    void delete(Long[] ids);

}