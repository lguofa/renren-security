/**
 * 
 *
 * 
 *
 * 
 */

package cn.eggsl.modules.sys.service;


import cn.eggsl.common.page.PageData;
import cn.eggsl.common.service.BaseService;
import cn.eggsl.modules.sys.dto.SysRoleDTO;
import cn.eggsl.modules.sys.entity.SysRoleEntity;

import java.util.List;
import java.util.Map;


/**
 * 角色
 * 
 * @author Mark sunlightcs@gmail.com
 */
public interface SysRoleService extends BaseService<SysRoleEntity> {

	PageData<SysRoleDTO> page(Map<String, Object> params);

	List<SysRoleDTO> list(Map<String, Object> params);

	SysRoleDTO get(Long id);

	void save(SysRoleDTO dto);

	void update(SysRoleDTO dto);

	void delete(Long[] ids);

}
