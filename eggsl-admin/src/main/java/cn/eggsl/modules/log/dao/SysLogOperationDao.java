/**
 *
 *
 *
 *
 *
 */

package cn.eggsl.modules.log.dao;

import cn.eggsl.common.dao.BaseDao;
import cn.eggsl.modules.log.entity.SysLogOperationEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 操作日志
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0
 */
@Mapper
public interface SysLogOperationDao extends BaseDao<SysLogOperationEntity> {
	
}
