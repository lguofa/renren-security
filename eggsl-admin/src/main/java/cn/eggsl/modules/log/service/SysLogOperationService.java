/**
 *
 *
 *
 *
 *
 */

package cn.eggsl.modules.log.service;

import cn.eggsl.common.page.PageData;
import cn.eggsl.common.service.BaseService;
import cn.eggsl.modules.log.dto.SysLogOperationDTO;
import cn.eggsl.modules.log.entity.SysLogOperationEntity;

import java.util.List;
import java.util.Map;

/**
 * 操作日志
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0
 */
public interface SysLogOperationService extends BaseService<SysLogOperationEntity> {

    PageData<SysLogOperationDTO> page(Map<String, Object> params);

    List<SysLogOperationDTO> list(Map<String, Object> params);

    void save(SysLogOperationEntity entity);
}