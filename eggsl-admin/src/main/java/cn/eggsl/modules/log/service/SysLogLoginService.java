/**
 * 
 *
 *
 *
 *
 */

package cn.eggsl.modules.log.service;

import cn.eggsl.common.page.PageData;
import cn.eggsl.common.service.BaseService;
import cn.eggsl.modules.log.dto.SysLogLoginDTO;
import cn.eggsl.modules.log.entity.SysLogLoginEntity;

import java.util.List;
import java.util.Map;

/**
 * 登录日志
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0
 */
public interface SysLogLoginService extends BaseService<SysLogLoginEntity> {

    PageData<SysLogLoginDTO> page(Map<String, Object> params);

    List<SysLogLoginDTO> list(Map<String, Object> params);

    void save(SysLogLoginEntity entity);
}