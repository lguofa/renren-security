/**
 *
 *
 *
 *
 *
 */

package cn.eggsl.modules.log.dao;

import cn.eggsl.common.dao.BaseDao;
import cn.eggsl.modules.log.entity.SysLogLoginEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 登录日志
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0
 */
@Mapper
public interface SysLogLoginDao extends BaseDao<SysLogLoginEntity> {
	
}
