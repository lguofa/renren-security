/**
 *
 *
 *
 *
 *
 */

package cn.eggsl.modules.security.service;

import cn.eggsl.common.service.BaseService;
import cn.eggsl.common.utils.Result;
import cn.eggsl.modules.security.entity.SysUserTokenEntity;

/**
 * 用户Token
 * 
 * @author Mark sunlightcs@gmail.com
 */
public interface SysUserTokenService extends BaseService<SysUserTokenEntity> {

	/**
	 * 生成token
	 * @param userId  用户ID
	 */
	Result createToken(Long userId);

	/**
	 * 退出，修改token值
	 * @param userId  用户ID
	 */
	void logout(Long userId);

}