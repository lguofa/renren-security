/**
 *
 * <p>
 *
 * <p>
 *
 */

package cn.eggsl;

import cn.hutool.core.util.StrUtil;
import cn.eggsl.common.redis.RedisUtils;
import cn.eggsl.modules.sys.entity.SysUserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {
    @Resource
    private RedisUtils redisUtils;

    @Test
    public void contextLoads() {
        SysUserEntity user = new SysUserEntity();
        user.setEmail("123456@qq.com");
        redisUtils.set("user", user);

        System.out.println(StrUtil.toString(redisUtils.get("user")));
    }

}